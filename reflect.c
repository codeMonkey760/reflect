// gcc -c ct.c -o ct.o  //build obj file but don't link
// objdump -d ct.o      //examine obj file ... it's easier to examine obj file than full exes
			//make sure that main ends in C9 C3, might depend on compiler and target arch
// gcc ct.o -o ct.elf   //finish the linking we skipped earlier
// ./ct.elf             // run the result

//assumes x86-64 arch

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main (int argc, char **argv) {
	int i, line = 0, count = 0;
	unsigned char *curByte = 0;
	unsigned long loc = 0;
	
	// print the location of main
	printf("main = 0x%016lX\n",(unsigned long) main);

	// print the compiled contents of main
	// ... assuming main ends in a C9 C3 leave ret combo
	loc = (unsigned long) main;
	curByte = (unsigned char *) main;
	printf("%016lX: ",loc+0);
	while (true) {
		if (*curByte == 0xC3 && *(curByte-1) == 0xC9) {
			break;
		}

		printf("%02X ", *curByte);
		curByte++;
		count++;
		if (count == 16) {
			count = 0;
			printf("\n");
			line++;
			printf("%016lX: ",loc+(line*16));
		}
	}
	printf("%02X\n",0xC3);

	return EXIT_SUCCESS;
}
